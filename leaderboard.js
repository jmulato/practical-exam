    new Vue({
  el: '#leaderboard',
  data: {
    currentPos: -1,
    items: [],
    boardCount: 10
  },
  created() {
    //initialize leaderboard
  	for(i = 1; i <= this.boardCount; i++) {
    	this.items.push(i);
    }
    
    alert('Please select a starting point');
  },
  methods: {
    /**
     * Catch keyboard event
     * 
     **/
  	changeLeaderBoard: function(event) {
      const ARROW_UP = 'ArrowUp';
      const ARROW_DOWN = 'ArrowDown';
      
      if (event.key === ARROW_UP) {
        if (this.currentPos > 0) {
          this.currentPos--      
        } else {
          alert('You are already at the top of leaderboard');
        }
      } else if(event.key === ARROW_DOWN) {
      	if (this.currentPos < (this.boardCount - 1)) {
        	this.currentPos++;
        } else {
        	alert('You are already at the bottom of leaderboard');
        }
      }
    },
    /**
     * Set current position when leaderboard is load
     * 
     **/
    setCurrentPos: function(index) {
    	if (this.currentPos < 0) {
    	  this.currentPos = index
    	  return this.currentPos;
      }
      
      return false;
    }
  },
  
})